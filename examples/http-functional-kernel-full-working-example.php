<?php

// use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
// use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollectionBuilder;

// use SymfonyPlayground\Component\HttpKernel\HttpKernel;

use function Functional\partial_left;
// use function SymfonyUtil\Component\HttpFoundation\send;

// replace with file to your own project bootstrap
if (file_exists(__DIR__ . '/../vendor/autoload.php')):
    include __DIR__ . '/../vendor/autoload.php';
else:
    require '/usr/share/php/Symfony/Component/EventDispatcher/autoload.php';
    require '/usr/share/php/Symfony/Component/Routing/autoload.php';
endif;

$request = Request::createFromGlobals();

// $dispatcher = new EventDispatcher();
// Why not fluid?
// $dispatcher->addSubscriber(new RouterListener(
    // new UrlMatcher(
        // (new RouteCollectionBuilder())
            // ->addRoute(new Route('/hello/{name}', [
                // '_controller' => function (Request $request) {
                    // return new Response(
                        // sprintf("Hello %s", $request->get('name'))
                    // );
                // }
            // ]))
            // ->build()
        // ,
        // new RequestContext()
    // )
    // ,
    // new RequestStack() #######################
// ));

$route_collection = (new RouteCollectionBuilder())
    ->addRoute(new Route('/hello/{name}', [
        '_controller' => function (Request $request) {
            return new Response(
                sprintf("Hello %s", $request->get('name'))
            );
        }
    ]))
    ->build()
;

$url_matcher = (new UrlMatcher(
    $route_collection,
    new RequestContext()
));

// https://github.com/symfony/http-kernel/blob/master/EventListener/RouterListener.php
function router(
    RequestMatcherInterface $url_matcher,
    Request $request
):Request {
    $request = clone $request;
    $parameters = $url_matcher->matchRequest($request);
    $request->attributes->add($parameters);
    unset($parameters['_route'], $parameters['_controller']);
    $request->attributes->set('_route_params', $parameters);
    return $request;
};

/**
 * Handles a throwable by trying to convert it to a Response.
 *
 * @throws \Exception
 */
// private
function handle_throwable(
    callable $response_event, // 5
    callable $finish_request_event, // 6
    callable $exception_event, // 8
    \Throwable $e,
    Request $request
): Response { // int $type
    // might replace the exception
    $exception_event_result = $exception_event(
        $request, $e
    ); // $this, $type
    $e = $exception_event_result['exception'];

    if (!array_key_exists(
        'response',
        $exception_event_result
    )) {
        $finish_request_event($request); // $this, $type !! Side-effect!!
        // $requestStack->pop(); // Modified for what?

        throw $e;
    }

    $response = $exception_event_result['response'];

    // the developer asked for a specific status code
    if (!$exception_event_result['isAllowingCustomResponseCode']
        && !$response->isClientError()
        && !$response->isServerError()
        && !$response->isRedirect()) {
        // ensure that we actually have an error response
        if ($e instanceof HttpExceptionInterface) {
            // keep the HTTP status code and headers
            $response->setStatusCode($e->getStatusCode());
            $response->headers->add($e->getHeaders());
        } else {
            $response->setStatusCode(500);
        }
    }

    try {
        // filter_response
        $response = $response_event(
            $request,
            $response
        ); // $type, $this

        $finish_request_event($request); // $this, $type !! Side-effect!!
        // $requestStack->pop(); // Modified for what?

        return $response;
    } catch (\Exception $e) {
        return $response;
    }
}

// $kernel = new HttpKernel(
    // $dispatcher,
    // new ControllerResolver(),
    // new RequestStack(),
    // new ArgumentResolver()
// );

interface ControllerResolverInterface
{
    // May also return a continuable or an error.
    // See https://github.com/symfony/symfony/blob/master/src/Symfony/Component/HttpKernel/Controller/ControllerResolverInterface.php
    public function __invoke(Request $request): callable;
}

interface ArgumentResolverInterface
{
    // May return an error
    // See https://github.com/symfony/symfony/blob/master/src/Symfony/Component/HttpKernel/Controller/ArgumentResolverInterface.php
    /**
     * Returns the arguments to pass to the controller.
     *
     * @return iterable<mixed> An array of arguments to pass to the controller
     */
    public function __invoke(Request $request, callable $controller): iterable;
}

interface RequestEventInterface // 1
{
    function __invoke(Request $request): Request;
}

interface ControllerEventInterface // 2
{
    public function __invoke(callable $controller, Request $request): callable;
}

interface ControllerArgumentsEventInterface // 3
{
    /**
     * @param iterable<mixed> $arguments
     * @return iterable<mixed> An array of arguments to pass to the controller
     */
    public function __invoke(callable $controller, iterable $arguments, Request $request): iterable;
}

interface HttpKernelInterface
{
    const b = 'Constant';
}

function http_kernel(
    RequestStack $requestStack,
    ControllerResolverInterface $controller_resolver,
    ArgumentResolverInterface $argument_resolver,
    RequestEventInterface $request_event, // 1
    ControllerEventInterface $controller_event, // 2
    ControllerArgumentsEventInterface $controller_arguments_event, // 3
    callable $view_event, // 4 unimplemented
    callable $response_event, // 5
    callable $finish_request_event, // 6
    callable $terminate_event, // 7 unimplemented
    callable $exception_event, // 8
    Request $request
): Response {
    $request->headers->set('X-Php-Ob-Level', (string) ob_get_level());

    try {

        $requestStack->push($request);

        $request = $request_event($request); // $type, $this
        // if ($event->hasResponse())...

        // load controller
        $controller = $controller_resolver($request);
        // NotFoundHttpException

        $controller = $controller_event(
            $controller,
            $request
        ); // $type, $this

        // controller arguments
        [
            'controller' => $controller,
            'arguments' => $arguments
        ] = $controller_arguments_event(
            $controller,
            $argument_resolver($request, $controller),
            $request
        ); // $type, $this

        // filter_response (view event skiped)
        $response = $response_event(
            $request,
            $controller(...$arguments)
        ); // $type, $this

        $finish_request_event($request); // $this, $type !! Side-effect!!
        $requestStack->pop(); // Modified for what?

        return $response;

    } catch (\Exception $e) {
        if ($e instanceof RequestExceptionInterface) {
            $e = new BadRequestHttpException($e->getMessage(), $e);
        }
        // if (false === $catch) { // $catch
            // $this->finishRequest($request, $type);

            // throw $e;
        // }

        return handle_throwable(
            $response_event, // 5
            $finish_request_event, // 6
            $exception_event, // 8
            $e,
            $request
        ); // $type
    }

}

// $kernel->terminate(
    // $request,
    // send($kernel->handle($request))
    // // ($kernel->handle($request)).send()
// );

http_kernel(
    new RequestStack(), // use?

    new class implements ControllerResolverInterface {
        function __invoke(Request $r): callable
        {
            $result = (new ControllerResolver())->getController($r); // non-static method
            if (false === $result) { exit("No controller found."); }
            return $result;
        }
    },

    // [(new ArgumentResolver()), 'getArguments'], // non-static method
    new class implements ArgumentResolverInterface {
        function __invoke(Request $r, callable $controller): iterable
        { return (new ArgumentResolver())->getArguments($r, $controller); } // non-static method
    },

    // partial_left($router, $url_matcher),
    new class($url_matcher) implements RequestEventInterface {  // 1 request
        private UrlMatcher $m;
        function __construct(UrlMatcher $m)
        { $this->m = $m; }
        function __invoke(Request $r): Request
        { return router($this->m, $r); }
    },

    new class implements ControllerEventInterface {  // 2 controller
        function __invoke(callable $controller, Request $r): callable
        { return $controller; }
    },

    new class implements ControllerArgumentsEventInterface {  // 3 arg
    /**
     * @param iterable<mixed> $arguments
     * @return iterable<mixed> An array of arguments to pass to the controller
     */
        function __invoke(callable $controller, iterable $arguments, Request $r): iterable
        {
            return [
                'controller' => $controller,
                'arguments' => $arguments
            ];
        }
    },

    function() {}, // 4 view unimplemented
    function($req, $res) {return $res;}, // 5 response event
    function($req) {return $req;}, // 6 finish request
    function() {}, // 7 terminate unimplemented
    function($req, $e) { // 8 exception
        return [
            'exception' => $e
        ];},
    $request
)->send();

